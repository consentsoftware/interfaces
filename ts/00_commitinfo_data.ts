/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@consentsoftware/interfaces',
  version: '1.0.13',
  description: 'the interfaces for consent.software'
}
